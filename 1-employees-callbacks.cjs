/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

// const data = {
//     "employees": [
//         {
//             "id": 23,
//             "name": "Daphny",
//             "company": "Scooby Doo"
//         },
//         {
//             "id": 73,
//             "name": "Buttercup",
//             "company": "Powerpuff Brigade"
//         },
//         {
//             "id": 93,
//             "name": "Blossom",
//             "company": "Powerpuff Brigade"
//         },
//         {
//             "id": 13,
//             "name": "Fred",
//             "company": "Scooby Doo"
//         },
//         {
//             "id": 89,
//             "name": "Welma",
//             "company": "Scooby Doo"
//         },
//         {
//             "id": 92,
//             "name": "Charles Xavier",
//             "company": "X-Men"
//         },
//         {
//             "id": 94,
//             "name": "Bubbles",
//             "company": "Powerpuff Brigade"
//         },
//         {
//             "id": 2,
//             "name": "Xyclops",
//             "company": "X-Men"
//         }
//     ]
// }


//=====================================================================================

const path = require("path");
const fs = require("fs");

const file = path.join(__dirname, "data.json");


function readFileFunction(file) {
    fs.readFile(file, "utf-8", (err, data) => {
        if (err) {
            console.error(err);
        } else {
            // console.log(data);
            const arr = JSON.parse(data)
            retriveDataForIds(arr);
        }
    });
};

readFileFunction(file);


// 1. Retrieve data for ids : [2, 13, 23].

function retriveDataForIds(arr) {
    let result = arr.employees.filter(obj => {
        const { id } = obj;
        return id === 2 || id === 13 || id === 23;
    });
    result = JSON.stringify(result, null, 2);
    fs.writeFile("./problem1.json", result, function (err) {
        if (err) {
            console.error(err);
        } else {
            gropData(arr);
        }
    })
}


// 2. Group data based on companies.
//         { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}

function gropData(arr) {
    let groupDataByCompany = arr.employees.reduce((newObj, obj) => {
        if (obj.company === "Scooby Doo") {
            newObj["Scooby Doo"].push(obj);
        } else if (obj.company === "Powerpuff Brigade") {
            newObj["Powerpuff Brigade"].push(obj);
        } else if (obj.company === "X-Men") {
            newObj["X-Men"].push(obj);
        }
        return newObj;
    }, { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": [] })
    // console.log(groupDataByCompany);
    groupDataByCompany = JSON.stringify(groupDataByCompany, null, 2);
    fs.writeFile("./problem2.json", groupDataByCompany, function (err) {
        if (err) {
            console.error(err);
        } else {
            getAllDataForPowerpuffBrigade(arr);
        }
    });
}




// 3. Get all data for company Powerpuff Brigade

function getAllDataForPowerpuffBrigade(arr) {
    // console.log(arr);
    let result = arr.employees.filter((obj) => {
        const { company } = obj;
        // console.log(company);
        return company === "Powerpuff Brigade";
    });

    result = JSON.stringify(result, null, 2);
    fs.writeFile("./problem3.json", result, function (err) {
        if (err) {
            console.error(err);
        } else {
            removeEntryWithIds(arr);
        }
    });
}


// 4. Remove entry with id 2.

function removeEntryWithIds(arr) {
    // console.log(arr);
    let result = arr.employees.filter((obj) => {
        return obj.id !== 2;
    });
    result = JSON.stringify(result, null, 2);
    fs.writeFile("./problem4.json", result, function (err) {
        if (err) {
            console.error(err);
        } else {
            sortData(arr);
        }
    });
}

// 5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.


function sortData(arr){
    // console.log(arr);
    let result = arr.employees.sort((obj1, obj2) => {
        return obj1.id - obj2.id;
    })
    result = result.sort((obj1, obj2) => {
        const company1 = obj1.company.toLowerCase();
        const company2 = obj2.company.toLowerCase();
        if(company1 > company2){
            return 1;
        }else if(company1 < company2){
            return -1;
        }else{
            return 0;
        }
    });
    
    // console.log(result);
    result = JSON.stringify(result, null, 2);
    fs.writeFile("./problem5.json", result, function (err) {
        if (err) {
            console.error(err);
        } else {
            swapPosition(arr);
        }
    });
}



// 6. Swap position of companies with id 93 and id 92.


function swapPosition(arr){
    let result = arr.employees.reduce((arr, obj, index) => {
        if(obj.id === 93 || obj.id === 92){
            arr.push(index);
        };
        return arr;
    },[]);
    
    let employees = arr.employees;
    [employees[result[0]], employees[result[1]]] = [employees[result[1]], employees[result[0]]];
    
    employees = JSON.stringify(employees, null, 2);
    fs.writeFile("./problem6.json", employees, function (err) {
        if (err) {
            console.error(err);
        } else {
            addingBirthdayIfIdEven(arr);
        }
    });
}




// 7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.



function addingBirthdayIfIdEven(arr){
    let result = arr.employees.map(obj => {
        if(obj.id % 2 === 0){
            const date = new Date().getDate();
            const month = new Date().getMonth() + 1;
            const year = new Date().getFullYear();
            
            obj.birthday = `${date}/${month}/${year}`;
        };
        return obj;
    });
    result = JSON.stringify(result, null, 2);
    fs.writeFile("./problem7.json", result, function (err) {
        if (err) {
            console.error(err);
        } else {
            console.log("Solved all given problems !");
        }
    });
}



//========================================================================================

